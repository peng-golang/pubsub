module gitlab.com/peng-golang/pubsub

go 1.21.0

require (
	github.com/ThreeDotsLabs/watermill v1.2.0-rc.6
	github.com/ThreeDotsLabs/watermill-amqp v1.1.4
	github.com/google/uuid v1.3.0
	github.com/redis/go-redis/v9 v9.1.0
	github.com/stretchr/testify v1.8.1
	go.uber.org/zap v1.25.0
)

require (
	github.com/cenkalti/backoff/v3 v3.2.2 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/lithammer/shortuuid/v3 v3.0.7 // indirect
	github.com/oklog/ulid v1.3.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/streadway/amqp v1.0.0 // indirect
	go.uber.org/multierr v1.10.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
