package pubsub_core

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNewSubject(t *testing.T) {
	subject := NewSubject[PubSubEvent]("test", 10).Start(context.Background())
	assert.NotEmpty(t, subject)
	ch := subject.Subscribe(uuid.NewString())
	subject.Publish(PubSubEvent{
		ID:        uuid.NewString(),
		Name:      "",
		Content:   []byte("{}"),
		TimeStamp: 0,
		Namespace: "",
	})

	e1 := <-ch
	assert.NotEmpty(t, e1)
	subject.Done()
}

func TestNewSubjectLong(t *testing.T) {
	subject := NewSubject[PubSubEvent]("test", 1000).StartWithMultiProcess(context.Background(), 1)
	assert.NotEmpty(t, subject)
	go func() {
		for i := 0; i < 1000; i++ {
			subject.Publish(PubSubEvent{
				ID:        uuid.NewString(),
				Name:      "",
				Content:   []byte("{}"),
				TimeStamp: int64(i),
				Namespace: "",
			})
			time.Sleep(time.Millisecond * 100)
		}
	}()

	for i := 0; i < 4; i++ {
		go func(id int) {
			topic := fmt.Sprintf("id-%d", id)
			ch := subject.Subscribe(topic)
			ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
			defer cancel()
			for {
				select {
				case <-ctx.Done():
					t.Logf("un sub")
					subject.Unsubscribe(topic)
					return
				case e := <-ch:
					t.Logf("%d subscriber %d,get event %d", time.Now().UnixMilli(), id, e.TimeStamp)
					//time.Sleep(time.Millisecond * 200)
				}
			}
		}(i)
	}

	time.Sleep(time.Second * 15)
}

func TestCloseChan(t *testing.T) {
	ch := make(chan int, 1)
	go func() {
		for i := 0; i < 100; i++ {
			ch <- i
		}
	}()
	HelperCloseChan(ch)
}
