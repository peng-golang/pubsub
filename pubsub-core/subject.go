package pubsub_core

import (
	"context"
	"log"
	"time"
)

type Subject[T interface{}] struct {
	subject chan T
	//mu          *sync.Mutex
	subscribers *SubscriberMap[T]
	BufferSize  int
	Name        string
	isClosed    bool
}

func NewSubject[T interface{}](name string, bufferSize int) *Subject[T] {
	return &Subject[T]{
		subject: make(chan T, bufferSize),
		//mu:          &sync.Mutex{},
		subscribers: NewSubscriberMap[T](),
		BufferSize:  bufferSize,
		Name:        name,
	}
}

func (subject *Subject[T]) Start(ctx context.Context) *Subject[T] {
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case msg := <-subject.subject:
				for _, subscriber := range subject.subscribers.GetAll() {
					if len(subscriber) >= subject.BufferSize {
						log.Printf("subject: %s,subscriber chan is full", subject.Name)
						continue
					}
					subscriber <- msg
				}
			}
		}
	}()
	return subject
}

func (subject *Subject[T]) StartWithMultiProcess(ctx context.Context, count int) *Subject[T] {
	for i := 0; i < count; i++ {
		go func() {
			for {
				select {
				case <-ctx.Done():
					return
				case msg := <-subject.subject:
					for key, subscriber := range subject.subscribers.GetAll() {
						if len(subscriber) >= subject.BufferSize {
							log.Printf("subject: %s, key: %s, subscriber chan is full", subject.Name, key)
							continue
						}
						subscriber <- msg
					}
				}
			}
		}()
	}
	return subject
}

func (subject *Subject[T]) Publish(event T) {
	if subject.isClosed {
		return
	}

	subject.subject <- event
}

func (subject *Subject[T]) Subscribe(id string) chan T {
	if ch, ok := subject.subscribers.TryGet(id); ok {
		return ch
	}
	subscriber := make(chan T, subject.BufferSize)
	subject.subscribers.Set(id, subscriber)
	return subscriber
}

func (subject *Subject[T]) Unsubscribe(id string) {
	if ch, ok := subject.subscribers.TryGet(id); ok {
		subject.subscribers.Delete(id)
		go HelperCloseChan(ch)
	}
}

func (subject *Subject[T]) SubCount() int {
	return subject.subscribers.Count()
}

func (subject *Subject[T]) Done() {
	subject.isClosed = true
	HelperCloseChan(subject.subject)
}

func HelperCloseChan[T interface{}](ch chan T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	for {
		select {
		case <-ctx.Done():
			close(ch)
			return
		case <-ch:
		}
	}
}
