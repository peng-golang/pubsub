package pubsub_core

import (
	"encoding/json"
	"github.com/google/uuid"
	"go.uber.org/zap"
	"time"
)

func SimpleToByte(t IDomainEvent) []byte {
	bs, _ := json.Marshal(t)
	return bs
}

func AddData(data map[string]interface{}) zap.Field {
	return zap.Any("data", data)
}

func ConvertToPubSubEvent(from IDomainEvent, namespace string) PubSubEvent {
	e := PubSubEvent{
		ID:        uuid.NewString(),
		Name:      from.GetName(),
		Content:   from.GetContent(),
		TimeStamp: time.Now().UnixMilli(),
		Namespace: namespace,
	}
	return e
}
