package pubsub_core

type IDomainEvent interface {
	GetNamespace() string
	GetName() string
	GetContent() []byte
}

type PubSubEvent struct {
	ID        string
	Name      string
	Content   []byte
	TimeStamp int64
	Namespace string
}

func (e PubSubEvent) GetNamespace() string {
	return e.Namespace
}

func (e PubSubEvent) GetName() string {
	return e.Name
}

func (e PubSubEvent) GetContent() []byte {
	return e.Content
}
