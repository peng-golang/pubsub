package pubsub_core

import "go.uber.org/zap"

type Dispatcher struct {
	eventMapping map[string]IEventHandler[IDomainEvent]
	logger       *zap.Logger
	isLog        bool
}

func NewDispatcher(
	logger *zap.Logger,
) *Dispatcher {
	return &Dispatcher{
		eventMapping: map[string]IEventHandler[IDomainEvent]{},
		logger:       logger,
		isLog:        true,
	}
}

func (d *Dispatcher) SetIsLog(isLog bool) {
	d.isLog = isLog
}

func (d *Dispatcher) AddEventHandler(handlers ...IEventHandler[IDomainEvent]) {
	for _, handler := range handlers {
		d.logger.Sugar().Infow("registering event handler", AddData(map[string]interface{}{
			"event": handler.EventName(),
		}))
		d.eventMapping[handler.EventName()] = handler
	}
}

func (d *Dispatcher) Dispatch(event IDomainEvent) {
	if d.isLog {
		d.logger.Sugar().Debugw("dispatching event", AddData(map[string]interface{}{
			"event": event.GetName(),
		}))
	}
	if handler, ok := d.eventMapping[event.GetName()]; ok {
		d.logger.Sugar().Debugw("handling event", AddData(map[string]interface{}{
			"event": event.GetName(),
		}))
		err := handler.Handle(event)
		if err != nil {
			d.logger.Sugar().Errorw("failed to handle event",
				AddData(map[string]interface{}{
					"event": event.GetName(),
				}), "error", err)
			return
		}
	}
}
