package pubsub_core

import "sync"

type SubscriberMap[T interface{}] struct {
	mu           *sync.Mutex
	subscribers  map[string]chan T
	subsSnapshot map[string]chan T
}

func NewSubscriberMap[T interface{}]() *SubscriberMap[T] {
	return &SubscriberMap[T]{
		mu:           &sync.Mutex{},
		subscribers:  make(map[string]chan T),
		subsSnapshot: make(map[string]chan T),
	}
}

func (m *SubscriberMap[T]) Set(key string, ch chan T) {
	m.mu.Lock()
	defer m.mu.Unlock()
	m.subscribers[key] = ch
	m.UpdateSnapshot()
}

func (m *SubscriberMap[T]) TryGet(key string) (chan T, bool) {
	m.mu.Lock()
	defer m.mu.Unlock()
	ctx, ok := m.subscribers[key]
	return ctx, ok
}

func (m *SubscriberMap[T]) Delete(key string) {
	m.mu.Lock()
	defer m.mu.Unlock()
	delete(m.subscribers, key)
	m.UpdateSnapshot()
}

func (m *SubscriberMap[T]) UpdateSnapshot() {
	temp := make(map[string]chan T)
	for k, v := range m.subscribers {
		temp[k] = v
	}
	m.subsSnapshot = temp
}

func (m *SubscriberMap[T]) GetAll() map[string]chan T {
	return m.subsSnapshot
}

func (m *SubscriberMap[T]) Count() int {
	m.mu.Lock()
	defer m.mu.Unlock()
	return len(m.subscribers)
}
