package pubsub_core

type IEventHandler[TEvent IDomainEvent] interface {
	EventName() string
	Handle(event TEvent) error
}
