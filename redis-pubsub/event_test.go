package redis_pubsub

import (
	pubsub_core "gitlab.com/peng-golang/pubsub/pubsub-core"
)

type TestEvent struct {
	Channel   string
	Index     int
	Timestamp int64
}

func (t TestEvent) GetNamespace() string {
	return "test"
}

func (t TestEvent) GetName() string {
	return "test-name"
}

func (t TestEvent) GetContent() []byte {
	return pubsub_core.SimpleToByte(t)
}
