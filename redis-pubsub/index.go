package redis_pubsub

import (
	"context"
	"encoding/json"
	"github.com/redis/go-redis/v9"
	pubsub_core "gitlab.com/peng-golang/pubsub/pubsub-core"
	"go.uber.org/zap"
)

type IPubsubService interface {
	Publish(channel string, event pubsub_core.IDomainEvent)
	SubscribeWithContext(ctx context.Context, channels []string, handler func(event *pubsub_core.PubSubEvent))
}

type PubsubService struct {
	namespace string
	rdb       *redis.Client
	logger    *zap.Logger
	ctx       context.Context
}

func NewPubsubService(ctx context.Context, namespace string, rdb *redis.Client, logger *zap.Logger) IPubsubService {
	return &PubsubService{
		namespace: namespace,
		rdb:       rdb,
		logger:    logger,
		ctx:       ctx,
	}
}

func (p *PubsubService) Publish(channel string, event pubsub_core.IDomainEvent) {
	payload, _ := json.Marshal(pubsub_core.ConvertToPubSubEvent(event, p.namespace))
	err := p.rdb.Publish(p.ctx, channel, payload)
	if err.Err() != nil {
		p.logger.Sugar().Errorw("redis publish error",
			"err", err.Err(),
			"data", map[string]interface{}{
				"channel": channel, "event": event,
			})
	}
}

func (p *PubsubService) SubscribeWithContext(ctx context.Context, channels []string, handler func(event *pubsub_core.PubSubEvent)) {
	pubsub := p.rdb.Subscribe(ctx, channels...)
	subChan := pubsub.Channel(redis.WithChannelSize(100))
	for {
		select {
		case <-ctx.Done():
			err := pubsub.Close()
			if err != nil {
				p.logger.Sugar().Errorw("redis subscribe close error",
					"err", err.Error(),
					"data", map[string]interface{}{
						"channels": channels,
					})
			}
			return
		case msg := <-subChan:
			go func(payload string) {
				event := new(pubsub_core.PubSubEvent)
				err := json.Unmarshal([]byte(payload), event)
				if err != nil {
					p.logger.Sugar().Errorw("json unmarshal error",
						"err", err.Error(),
						"data", map[string]interface{}{
							"channels": channels,
						})
				}
				handler(event)
			}(msg.Payload)
		}
	}

}
