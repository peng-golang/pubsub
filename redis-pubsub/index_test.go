package redis_pubsub

import (
	"context"
	"encoding/json"
	"github.com/redis/go-redis/v9"
	pubsub_core "gitlab.com/peng-golang/pubsub/pubsub-core"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"testing"
	"time"
)

func GetConsoleEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	return zapcore.NewJSONEncoder(encoderConfig)
}
func InitConsoleLog() *zap.Logger {
	encoder := GetConsoleEncoder()
	writer := zapcore.AddSync(os.Stdout)
	core := zapcore.NewCore(encoder, writer, zapcore.InfoLevel)
	return zap.New(core)
}

type handler struct {
	logger *zap.Logger
}

func (h handler) EventName() string {
	return TestEvent{}.GetName()
}

func (h handler) Handle(event pubsub_core.IDomainEvent) error {
	content := &TestEvent{}
	json.Unmarshal(event.GetContent(), content)
	h.logger.Sugar().Infow("get event", "event", content, "tsDiff", time.Now().UnixMilli()-content.Timestamp)
	return nil
}

func TestPubsubService_Subscribe(t *testing.T) {
	logger := InitConsoleLog()
	rdb := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
		DB:   0,
	})
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ps := NewPubsubService(ctx, "test", rdb, logger)
	go func() {
		ps.SubscribeWithContext(
			context.Background(),
			[]string{"test1"},
			func(event *pubsub_core.PubSubEvent) {
				content := &TestEvent{}
				json.Unmarshal(event.GetContent(), content)
				logger.Sugar().Infow("get event", "event", content, "tsDiff", time.Now().UnixMilli()-content.Timestamp)
			},
		)
	}()

	go func() {
		ps.SubscribeWithContext(
			context.Background(),
			[]string{"test2"},
			func(event *pubsub_core.PubSubEvent) {
				content := &TestEvent{}
				json.Unmarshal(event.GetContent(), content)
				logger.Sugar().Infow("get event", "event", content, "tsDiff", time.Now().UnixMilli()-content.Timestamp)
			},
		)
	}()

	for i := 0; i < 100; i++ {
		go func(index int) {
			ps.Publish("test1", TestEvent{
				Channel:   "c1",
				Index:     index,
				Timestamp: time.Now().UnixMilli(),
			})
		}(i)
		go func(index int) {
			ps.Publish("test2", TestEvent{
				Channel:   "c2",
				Index:     index,
				Timestamp: time.Now().UnixMilli(),
			})
		}(i)

	}
	time.Sleep(time.Second * 10)
}
