package pubsub

import (
	"context"
	"encoding/json"
	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-amqp/pkg/amqp"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/google/uuid"
	pubsub_core "gitlab.com/peng-golang/pubsub/pubsub-core"
	"go.uber.org/zap"
	"time"
)

type IPubsubService interface {
	Publish(event pubsub_core.IDomainEvent)
	Subscribe(namespace string, ctx context.Context, dispatcher *pubsub_core.Dispatcher) error
}

type PubsubService struct {
	Namespace string
	pub       *amqp.Publisher
	sub       *amqp.Subscriber
	logger    *zap.Logger
}

func (p *PubsubService) Publish(event pubsub_core.IDomainEvent) {
	e := pubsub_core.PubSubEvent{
		ID:        uuid.NewString(),
		Name:      event.GetName(),
		Content:   event.GetContent(),
		TimeStamp: time.Now().UnixMilli(),
		Namespace: p.Namespace,
	}
	payload, _ := json.Marshal(e)
	msg := message.NewMessage(e.ID, payload)
	_ = p.pub.Publish(p.Namespace, msg)
}

func (p *PubsubService) Subscribe(namespace string, ctx context.Context, dispatcher *pubsub_core.Dispatcher) error {
	msgChan, err := p.sub.Subscribe(ctx, namespace)
	if err != nil {
		return err
	}
	for {
		select {
		case <-ctx.Done():
			p.logger.Sugar().Infow("Pubsub service stopped", pubsub_core.AddData(map[string]interface{}{
				"namespace": namespace,
			}))
			return nil
		case msg := <-msgChan:
			msg.Ack()
			pubsubEvent, err := ParseMsgToEvent(msg)
			if err != nil {
				p.logger.Sugar().Errorw("Error parsing message", "err", err)
				continue
			}
			go dispatcher.Dispatch(pubsubEvent)
		}
	}

}

type SubscribeWithChannelOption struct {
	ChannelNum int
}

func (p *PubsubService) SubscribeWithChannel(namespace string, ctx context.Context, dispatcher *pubsub_core.Dispatcher, option SubscribeWithChannelOption) error {
	msgChan, err := p.sub.Subscribe(ctx, namespace)
	if err != nil {
		return err
	}
	cn := option.ChannelNum
	if option.ChannelNum == 0 {
		cn = 1
	}
	subChan := make(chan pubsub_core.PubSubEvent, option.ChannelNum)
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case msg := <-msgChan:
				msg.Ack()
				pubsubEvent, err := ParseMsgToEvent(msg)
				if err != nil {
					p.logger.Sugar().Errorw("Error parsing message", "err", err)
					continue
				}
				subChan <- pubsubEvent
			}
		}
	}()
	for i := 0; i < cn; i++ {
		go func(id int) {
			for {
				select {
				case <-ctx.Done():
					p.logger.Sugar().Infow("Pubsub service stopped", pubsub_core.AddData(map[string]interface{}{
						"namespace": namespace,
						"channel":   id,
					}))
					return
				case msg := <-subChan:
					dispatcher.Dispatch(msg)
				}
			}
		}(i)
	}
	<-ctx.Done()
	return nil
}

func NewPubsubService(rabbitMqUrl string, namespace string, logger *zap.Logger) *PubsubService {
	config := amqp.NewDurablePubSubConfig(rabbitMqUrl, nil)
	pub, err := amqp.NewPublisher(config, watermill.NewStdLogger(false, false))
	if err != nil {
		panic(err)
	}
	sub, err := amqp.NewSubscriber(
		amqp.NewDurablePubSubConfig(rabbitMqUrl, amqp.GenerateQueueNameTopicNameWithSuffix(namespace)),
		watermill.NewStdLogger(false, false),
	)
	return &PubsubService{
		pub:       pub,
		sub:       sub,
		Namespace: namespace,
		logger:    logger,
	}
}
