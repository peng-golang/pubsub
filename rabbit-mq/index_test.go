package pubsub

import (
	"context"
	pubsub_core "gitlab.com/peng-golang/pubsub/pubsub-core"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"testing"
	"time"
)

func GetConsoleEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	return zapcore.NewJSONEncoder(encoderConfig)
}
func InitConsoleLog() *zap.Logger {
	encoder := GetConsoleEncoder()
	writer := zapcore.AddSync(os.Stdout)
	core := zapcore.NewCore(encoder, writer, zapcore.DebugLevel)
	return zap.New(core)
}

type handler struct {
}

func (h handler) EventName() string {
	return TestEvent{}.GetName()
}

func (h handler) Handle(event pubsub_core.IDomainEvent) error {
	return nil
}

func TestPubsubService_Publish(t *testing.T) {
	logger := InitConsoleLog()
	ps := NewPubsubService("amqp://admin:admin@localhost:5672", "test", logger)

	go func() {
		dispatcher := pubsub_core.NewDispatcher(logger)
		h := handler{}
		dispatcher.AddEventHandler(h)
		err := ps.Subscribe("test", context.Background(), dispatcher)
		if err != nil {
			logger.Error("subscribe error", zap.Error(err))
		}
	}()
	for i := 0; i < 10; i++ {
		ps.Publish(TestEvent{
			F1: "f1",
			F2: 1,
		})
		time.Sleep(1 * time.Second)
	}
}
