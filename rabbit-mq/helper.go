package pubsub

import (
	"encoding/json"
	"github.com/ThreeDotsLabs/watermill/message"
	pubsub_core "gitlab.com/peng-golang/pubsub/pubsub-core"
)

func ParseMsgToEvent(msg *message.Message) (pubsub_core.PubSubEvent, error) {
	event := new(pubsub_core.PubSubEvent)
	err := json.Unmarshal(msg.Payload, event)
	return *event, err
}
