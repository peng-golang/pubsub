package pubsub

import pubsub_core "gitlab.com/peng-golang/pubsub/pubsub-core"

type TestEvent struct {
	F1 string
	F2 int
}

func (t TestEvent) GetNamespace() string {
	return "test"
}

func (t TestEvent) GetName() string {
	return "test-name"
}

func (t TestEvent) GetContent() []byte {
	return pubsub_core.SimpleToByte(t)
}
